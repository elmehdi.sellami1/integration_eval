@echo off

if "%1"=="" (
    echo Usage: %0 [nom_du_fichier_zip] [répertoire_à_zipper]
    exit /b 1
)

set FILENAME=%1
set SOURCE_DIR=%2

if not exist "%SOURCE_DIR%" (
    echo Le répertoire spécifié n'existe pas.
    exit /b 1
)

if exist "%FILENAME%" (
    echo Le fichier %FILENAME% existe déjà. Veuillez choisir un autre nom.
    exit /b 1
)

echo Zippage du répertoire %SOURCE_DIR% vers %FILENAME% en cours...

7z a -r -tzip "%FILENAME%" "%SOURCE_DIR%"

if %errorlevel% neq 0 (
    echo Erreur lors du zippage.
    exit /b 1
)

echo Artefact de livraison généré avec succès: %FILENAME%
exit /b 0
