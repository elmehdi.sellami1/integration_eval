
FROM ubuntu:latest
RUN apt-get update && \
    apt-get install -y apache2

COPY /Sapache /var/www/html

EXPOSE 82
# Commande à exécuter lors du démarrage du conteneur
CMD ["apache2ctl", "-D", "FOREGROUND"]
