@echo off
if "%1"=="start" (
    java -jar jenkins.war --httpPort=9090 --prefix=/jenkins
    echo Jenkins a été démarré.
) else if "%1"=="stop" (
    taskkill /F /IM java.exe /T
    echo Arrêt de Jenkins en cours...
) else (
    echo Usage: %0 [start|stop]
    exit /b 1
)
